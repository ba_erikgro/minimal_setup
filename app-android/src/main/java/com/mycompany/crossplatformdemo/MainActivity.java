package com.mycompany.crossplatformdemo;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import io.nevernull.mobileui.android.MobileUILayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainController mainController = new MainController();

        Log.e("ETESTSE", System.getProperty("java.library.path"));

        setContentView(new MobileUILayout(this, "Main.layout.xml", mainController));
    }
}
