package com.mycompany.crossplatformdemo;

import io.nevernull.mobileui.android.MobileUIAndroid;

public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        MobileUIAndroid.init(this, false);
    }
}
