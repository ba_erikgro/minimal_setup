package com.mycompany.crossplatformdemo;

import com.mycompany.nativelib.NativeLibrary;

import io.nevernull.mobileui.ios.MobileUIIos;
import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.foundation.NSBundle;
import org.robovm.apple.uikit.*;

public class Main extends UIApplicationDelegateAdapter {
    private UIWindow window;
    private MainUIViewController rootViewController;

    @Override
    public boolean didFinishLaunching(UIApplication application, UIApplicationLaunchOptions launchOptions) {
        MobileUIIos.initComponent();

        // Set up the view controller.
        rootViewController = new MainUIViewController();

        // Create a new window at screen size.
        window = new UIWindow(UIScreen.getMainScreen().getBounds());

        UINavigationController navController = new UINavigationController(rootViewController);
        navController.getNavigationBar().setTranslucent(false);

        // Set the view controller as the root controller for the window.
        window.setRootViewController(navController);
        // Make the window visible.
        window.makeKeyAndVisible();

        return true;
    }

    public static void main(String[] args) {
        try (NSAutoreleasePool pool = new NSAutoreleasePool()) {
            UIApplication.main(args, null, Main.class);
        }
    }
}
