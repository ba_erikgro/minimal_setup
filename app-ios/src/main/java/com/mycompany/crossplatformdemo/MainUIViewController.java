package com.mycompany.crossplatformdemo;

import org.robovm.apple.uikit.UIViewController;
import io.nevernull.mobileui.ios.MobileUILayout;

public class MainUIViewController extends UIViewController {

    @Override
    public void loadView() {
        super.loadView();
        setTitle("crossplatformdemo");
        MainController mainController = new MainController();
        setView(new MobileUILayout(this, "Main.layout.xml", mainController));
    }
}
