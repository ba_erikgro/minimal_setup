# Minimal setup for mobile cross platform integration of C++ for Android/RoboVM-iOS
iOS and Android example project using a common C++-Library with an automatically generated Java API via [JavaCPP](https://github.com/bytedeco/javacpp).

### Setup
- macOS (Tested on 10.15)
- Xcode and Xcode Command Line Tools installed
- JDK 8 or newer installed and JAVA_HOME set
- Android NDK available and ANDROID_NDK_HOME set (Tested on ndk r21d) (Can be installed via module settings in Android Studio or [Download](https://developer.android.com/ndk/downloads))
- MobileUI and RoboVM Plugin for Android Studio installed ([Download](https://mobileui.dev/login/))
- You may have to adjust roboVM and mobileUI versions in ./build.gradle file to match your installed versions.
- Import project via Import option in Android Studio & switch in Android Studio from 'Android' to 'Project' view in upper left corner, in order to see all files relevant for building the libraries.

### Running iOS/Android apps
Both libraries for Android and iOS are prebuilt in the projects via the build script 'app-common/build_cpp.sh'.
Thus you can run both Android and iOS App without building the libraries. 
To run the apps add run configurations for Android/RoboVM in Android Studio. You might have to specify the correct module for Android/iOS in run configurations.

In order to run on iOS device, you have to register the app.id in app-ios/robovm.properties.
The easiest way is to create an iPhone app in XCode with the same bundle id like the app.id in app-ios/robovm.properties.
Then you have to start the app in XCode and trust your development machine on your device.
After that you should be able to launch the app with the same app.id in Android Studio. 
Further information is provided at [mobidevelop homepage](http://robovm.mobidevelop.com/docs/en/getting-started/provisioning.html).

### Building iOS/Android libraries & generate Java API
When executing Android toolchain, gatekeeper prompts that the tools from Android NDK are not from a verified developer.
Since a lot of programs are executed the easiest solution is to temporarily disable gatekeeper via:
`sudo spctl --master-disable`
You can enable it after building via:
`sudo spctl --master-enable`

The following script does 3 things:
1. Generate gluecode for C++ via JavaCPP (build_gluecode.sh)
2. Build static fat library for iOS (build_iOS.sh)
3. Build shared libraries for Android (build_Android.sh)

```console
foo@bar:~$ bash app-common/build_cpp.sh 
```

More details are in the comments of the scripts.
