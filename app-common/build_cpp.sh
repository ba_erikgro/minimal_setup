#!/bin/bash

set -e
cd "$(dirname "$0")"

echo -e "\nGenerating gluecode via JavaCPP...\n"
bash ./build_gluecode.sh

echo -e "\n\n\nStarting Build for iOS..."
bash ./build_iOS.sh

echo -e "\n\n\nStarting Build for Android..."
bash ./build_Android.sh