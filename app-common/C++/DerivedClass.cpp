//
//  ClassB.cpp
//  C++App
//
//  Created by Erik Großkopf on 15.06.20.
//  Copyright © 2020 Erik Großkopf. All rights reserved.
//

#include "DerivedClass.hpp"

namespace demo {

    void DerivedClass::printHello() {
        std::cout << "\nHello From B" << std::endl;
    }

    std::string DerivedClass::getGreetingString() {
        return "Hello from class 'DerivedClass'!";
    }

} // namespace demo
