//
//  ClassA.hpp
//  C++App
//
//  Created by Erik Großkopf on 15.06.20.
//  Copyright © 2020 Erik Großkopf. All rights reserved.
//

#ifndef BaseClass_hpp
#define BaseClass_hpp

#include <string>

namespace demo {

    class BaseClass {
    public:
        virtual void printHello() = 0;
        virtual std::string getGreetingString() = 0;
        virtual ~BaseClass(){};
    };

}

#endif /* BaseClass_hpp */
