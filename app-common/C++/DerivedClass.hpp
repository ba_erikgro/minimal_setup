//
//  ClassB.hpp
//  C++App
//
//  Created by Erik Großkopf on 15.06.20.
//  Copyright © 2020 Erik Großkopf. All rights reserved.
//

#ifndef DerivedClass_hpp
#define DerivedClass_hpp

#include "BaseClass.hpp"
#include <iostream>

namespace demo {

    class DerivedClass : public BaseClass {
    public:
        void printHello() override;
        std::string getGreetingString() override;
        ~DerivedClass() override {};
    };

}

#endif /* DerivedClass_hpp */
