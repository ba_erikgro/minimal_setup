//
//  BasicDataTypesTest.hpp
//  C++App
//
//  Created by Erik Großkopf on 15.06.20.
//  Copyright © 2020 Erik Großkopf. All rights reserved.
//

#ifndef BasicDataTypesTest_hpp
#define BasicDataTypesTest_hpp

#include <iostream>
#include <string>
#include "jni.h"

namespace demo {

    class BasicDataTypesTest {
    public:
        bool boolValue;
        char charValue;
        short shortValue;
        float floatValue;
        double doubleValue;
        
        BasicDataTypesTest();
        
        void printAllDataTypes();
        std::string getAllDataTypesAsString();
    };

}

#endif /* BasicDataTypesTest_hpp */
