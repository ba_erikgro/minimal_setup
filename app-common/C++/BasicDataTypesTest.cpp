//
//  BasicDataTypesTest.cpp
//  C++App
//
//  Created by Erik Großkopf on 15.06.20.
//  Copyright © 2020 Erik Großkopf. All rights reserved.
//

#include "BasicDataTypesTest.hpp"

namespace demo {

    BasicDataTypesTest::BasicDataTypesTest() {
        boolValue = true;
        charValue = 'A';
        shortValue = 1;
        floatValue = 3.141592;
        doubleValue = 2.71828182846;
    }

    void BasicDataTypesTest::printAllDataTypes() {
        std::cout << "### Basic data types test ###"
        << "\nbool: " << boolValue
        << "\nchar: " << charValue
        << "\nshort: " << shortValue
        << "\nfloat: " << floatValue
        << "\ndouble: " << doubleValue
        << std::endl;
    }

    std::string BasicDataTypesTest::getAllDataTypesAsString() {
        std::string retVal = "\nbool: ";
        retVal.append(boolValue ? "true" : "false");
        retVal.append("\nchar: ");
        retVal.append(std::string(1, charValue));
        retVal.append("\nshort: ");
        retVal.append(std::to_string(shortValue));
        retVal.append("\nfloat: ");
        retVal.append(std::to_string(floatValue));
        retVal.append("\ndouble: ");
        retVal.append(std::to_string(doubleValue));
        
        return retVal;
    }

}
