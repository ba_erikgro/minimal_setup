#include <jni.h>

#ifndef _NativeHelper
#define _NativeHelper
#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jint JNICALL JNI_OnLoad_jniNativeLibrary(JavaVM* vm, void* reserved);

//static JavaVM* JavaCPP_vm = NULL;
static JavaVM* tmp_vm = NULL;

/*
 * Class:     com_mycompany_crossplatformdemo_NativeHelper
 * Method:    initJavaCPP
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_mycompany_crossplatformdemo_NativeHelper_initJavaCPP(JNIEnv * env, jclass) {
    env->GetJavaVM(&tmp_vm);
    void* ptr;
    JNI_OnLoad_jniNativeLibrary(tmp_vm, ptr);
}

#ifdef __cplusplus
}
#endif
#endif
