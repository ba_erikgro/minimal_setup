package com.mycompany.crossplatformdemo;

import com.mycompany.nativelib.NativeLibrary;

public class MainController {
    public String greeting = "";
    public String basicDatatypes = "";
    public String inheritance = "";

    public MainController() {
        // Setting up explenation label
        greeting = "This Android/iOS-RoboVM demo app shows the usage " +
                "of a generated Java API for C++ Code with the framework 'JavaCPP'. " +
                "Both Java classes 'BasicDataTypesTest' and 'DerivedClass' invoke native " +
                "C++ functions via JNI...";

        // Testing primitive datatypes and strings from c++ standard library libc++
        basicDatatypes = "Basic datatypes test:";
        NativeLibrary.BasicDataTypesTest test = new NativeLibrary.BasicDataTypesTest();
        basicDatatypes += test.getAllDataTypesAsString().getString();

        // Testing basic inheritance, virtual functions and polymorphism
        inheritance = "Basic inheritance/polymorphism test:\n";
        NativeLibrary.BaseClass derivedClass = new NativeLibrary.DerivedClass();
        inheritance += derivedClass.getGreetingString().getString();
    }
}