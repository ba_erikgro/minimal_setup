package com.mycompany.nativelib;

import org.bytedeco.javacpp.annotation.*;
import org.bytedeco.javacpp.tools.*;

@Properties(
        value = @Platform(
                compiler = "cpp11",
                includepath = "../../../C++/",
                include = {"BaseClass.hpp",
                            "DerivedClass.hpp",
                            "BasicDataTypesTest.hpp"}
        ),
        target = "com.mycompany.nativelib.NativeLibrary"
)
@Namespace("demo")
public class NativeLibraryConfig implements InfoMapper {
    public void map(InfoMap infoMap) {
    }
}