#!/bin/bash
# This script generates Java/C++ gluecode for Android/iOS via JavaCPP
# The configuration for the generation is defined in ‘NativeLibraryConfig.java‘


set -e
cd "$(dirname "$0")"

# JacaCPP needs to be executed from java source directory
# since the target in NativeLibraryConfig is defined by relative path
cd ./src/main/java/

# Generate Java-gluecode for C++-Header
java -jar ../../../javacpp.jar com/mycompany/nativelib/NativeLibraryConfig.java



#### iOS gluecode generation
# Generate C++-JavaCPP-Gluecode
java -jar ../../../javacpp.jar com/mycompany/nativelib/*.java -nocompile -properties ios-x86_64

# Remove .class files
rm -f ./com/mycompany/nativelib/*.class

# Move C++ files to new directory
mkdir -p ../../../C++_ios
mv ./*.mm ../../../C++_ios
mv ./com/mycompany/nativelib/*.mm ../../../C++_ios

# Remove unnecesary library directory
rm -rf ./com/mycompany/nativelib/ios-x86_64



#### Android gluecode generation
# Generate C++-JavaCPP-Gluecode
java -jar ../../../javacpp.jar com/mycompany/nativelib/*.java -nocompile -properties android-x86-clang

# Remove .class files
rm ./com/mycompany/nativelib/*.class

# Move C++ files to new directory
mkdir -p ../../../C++_android
mv ./*.cpp ../../../C++_android
mv ./com/mycompany/nativelib/*.cpp ../../../C++_android

# Remove unnecessary library directory
rm -rf ./lib/

echo "Gluecode created ✔"