#!/bin/bash
# This script builds a static fat library for iOS.
# The source code for the library lies in ./C++/ and ./C++_ios.
# The created lib is saved to to app-ios/libs/
# You can adjust the C++ source code and just run this script to access its generated Java API.

set -e
cd "$(dirname "$0")"



########## CHECKING IF ALL TOOLS AVAILABLE ##########
echo -e "Checking tools..."
failed=false
if [[ -z $JAVA_HOME ]]; then
    echo "Environment variable JAVA_HOME must be set."
    failed=true
else 
    echo "JAVA_HOME ✔"
fi

if [[ $(xcode-select -p 1>/dev/null;echo $?) == 2 ]]; then
    echo "XCode Command Line Tools not installed."
    failed=true
else 
    echo "XCode Command Line Tools ✔"
fi

if [[ $failed == true ]]; then 
    exit 1
fi



########## SETTING UP THE BUILD COMMANDS ##########
# Clang++ compiler from ios device sdk toolchain
CLANG_DEVICE=$(xcrun --sdk iphoneos --find clang++)

# Clang++ compiler from ios sim sdk toolchain
CLANG_SIM=$(xcrun --sdk iphonesimulator --find clang++)

# Set includes for jni.h and jni_md.h from JDK
JNI_INCLUDES="-I${JAVA_HOME}/include -I${JAVA_HOME}/include/darwin"

# Set sysroot for iOS sim
SYSROOT_SIM=$(xcrun --sdk iphonesimulator --show-sdk-path)

# Set sysroot for iOS device
SYSROOT_DEVICE=$(xcrun --sdk iphoneos --show-sdk-path)

# Set C++ source files
SOURCES="./C++/*.cpp ./C++_ios/*.mm"

API_INCLUDE="-I./C++/"

# Archive tool for building static libraries
ARCHIVER=$(xcrun --sdk iphoneos --find ar)

IOS_LIBS="../app-ios/libs/"



########## EXECUTING BUILD COMMANDS ##########
echo -e "\nStarting the build..."
# Cleanup
rm -f ./*.o
rm -f ./*.a

eval $CLANG_SIM -std=c++11 $JNI_INCLUDES $API_INCLUDE -D__APPLE__ -D__OBJC__  -isysroot $SYSROOT_SIM -arch x86_64 -c $SOURCES
eval $ARCHIVER rcs lib_demo_static_simulator.a *.o
echo "Created library for iOS simulator ✔"
rm -f ./*.o

# Build static library for iOS device with arm64
eval $CLANG_DEVICE -std=c++11 $JNI_INCLUDES $API_INCLUDE -D__APPLE__ -D__OBJC__ -isysroot $SYSROOT_DEVICE -arch arm64 -c $SOURCES
eval $ARCHIVER rcs lib_demo_static_device.a *.o
echo "Created library for iOS device ✔"
rm -f ./*.o

# Create fat library
echo "Fat static library for iOS (x86/arm64) created: ${IOS_LIBS}demofatlib.a ✔"
lipo -create *.a -output "${IOS_LIBS}demofatlib.a"

# Cleanup
rm -f ./*.a
