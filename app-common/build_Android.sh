#!/bin/bash
# This script builds shared libraries for Android.
# The source code for the library lies in ./C++/ and ./C++_android.
# The created libs are saved to /app-android/src/main/jniLibs
# You can adjust the C++ source code and just run this script to access its generated Java API.

set -e
cd "$(dirname "$0")"



########## CHECKING IF ALL TOOLS AVAILABLE ##########
echo -e "Checking tools..."
failed=false
if [[ -z $JAVA_HOME ]]; then
    echo "Environment variable JAVA_HOME must be set."
    failed=true
else 
    echo "JAVA_HOME ✔"
fi
if [[ -z $ANDROID_NDK_HOME ]]; then
    echo "Environment variable ANDROID_NDK_HOME must be set."
    failed=true
else 
    echo "ANDROID_NDK_HOME ✔"
fi

if [[ $failed == true ]]; then 
    exit 1
fi



########## SETTING UP THE BUILD COMMANDS ##########
# Clang++ compiler from NDK
CLANG_ANDROID="$ANDROID_NDK_HOME/toolchains/llvm/prebuilt/darwin-x86_64/bin/clang++"

# Define targets
TARGET_X86="-target i686-linux-android29"
TARGET_ARM="-target aarch64-linux-android29"

# Set includes for jni.h and jni_md.h from JDK
JNI_INCLUDES="-I${JAVA_HOME}include -I${JAVA_HOME}include/darwin"

# Define sysroot of android
SYSROOT="-isysroot ${ANDROID_NDK_HOME}/sysroot/"

API_INCLUDE="-IC++/"

# Set C++ source files
SOURCES="./C++/*.cpp ./C++_android/*.cpp"

PATH_EMULATOR="../app-android/src/main/jniLibs/x86/"
PATH_DEVICE="../app-android/src/main/jniLibs/arm64-v8a/"



########## EXECUTING BUILD COMMANDS ##########
echo -e "\nStarting the build..."
rm -f ./*.o

mkdir -p $PATH_EMULATOR
mkdir -p $PATH_DEVICE

# Build dynamic library for Android emulator x86 
eval $CLANG_ANDROID $TARGET_X86 $SYSROOT $JNI_INCLUDES $API_INCLUDE $SOURCES -fPIC -llog -D__ANDROID__  -shared -o "${PATH_EMULATOR}libjniNativeLibrary.so"
echo "Shared library for Android emulator (x86) created: ${PATH_EMULATOR}libjniNativeLibrary.so ✔"

# Build dynamic library for Android device arm64
eval $CLANG_ANDROID $TARGET_ARM $SYSROOT $JNI_INCLUDES $API_INCLUDE $SOURCES -fPIC -llog -D__ANDROID__ -shared -o "${PATH_DEVICE}libjniNativeLibrary.so"
echo "Shared library for Android device (arm64) created: ${PATH_DEVICE}libjniNativeLibrary.so ✔"

# Copying libc++ to android project...
# These paths might have to be adjusted depending on your installed NDK
cp $ANDROID_NDK_HOME/sources/cxx-stl/llvm-libc++/libs/x86/libc++_shared.so $PATH_EMULATOR
cp $ANDROID_NDK_HOME/sources/cxx-stl/llvm-libc++/libs/arm64-v8a/libc++_shared.so $PATH_DEVICE


########## CLEANUP ##########
rm -rf native/
rm -f ./*.o